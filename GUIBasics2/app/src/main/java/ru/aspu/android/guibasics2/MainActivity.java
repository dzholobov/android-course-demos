package ru.aspu.android.guibasics2;

import android.app.Activity;
import android.os.Bundle;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setTitle("GUIBasics2");
		LinearLayout main = new LinearLayout(this);
		setContentView(main);
		TextView outputTextView = new TextView(this);
		main.addView(outputTextView);
		LinearLayout buttonsLayout = new LinearLayout(this);
		main.addView(buttonsLayout);
		Button firstButton = new Button(this);
		buttonsLayout.addView(firstButton);
		Button secondButton = new Button(this);
		buttonsLayout.addView(secondButton);

		main.setOrientation(LinearLayout.VERTICAL);
		main.getLayoutParams().width = LayoutParams.MATCH_PARENT;
		main.getLayoutParams().height = LayoutParams.MATCH_PARENT;
		outputTextView.setText("Текст");
		outputTextView.getLayoutParams().width = LayoutParams.MATCH_PARENT;
		outputTextView.getLayoutParams().height = 150;
		buttonsLayout.setOrientation(LinearLayout.HORIZONTAL);
		buttonsLayout.getLayoutParams().width = LayoutParams.MATCH_PARENT;
		buttonsLayout.getLayoutParams().height = LayoutParams.WRAP_CONTENT;
		firstButton.setText("Первая кнопка");
		firstButton.getLayoutParams().width = LayoutParams.WRAP_CONTENT;
		firstButton.getLayoutParams().height = LayoutParams.WRAP_CONTENT;
		secondButton.setText("Вторая кнопка");
		secondButton.getLayoutParams().width = LayoutParams.WRAP_CONTENT;
		secondButton.getLayoutParams().height = LayoutParams.WRAP_CONTENT;
	}

}
