package ru.aspu.android.controlsdemo1;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.Switch;
import android.widget.Toast;
import android.widget.ToggleButton;

public class MainActivity extends Activity {

	private String selectedTransport;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        RadioGroup sampleRadioGroup = (RadioGroup)findViewById(R.id.sampleRadioGroup);
        sampleRadioGroup.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(RadioGroup group, int checkedId) {
				RadioButton newChecked = (RadioButton)findViewById(checkedId);
				selectedTransport = newChecked.getText().toString();
			}
		});
    }

    public void sampleButtonClick(View view) {
    	Toast.makeText(this, R.string.sampleButtonMessage, Toast.LENGTH_SHORT).show();
    }
    
    public void sampleToggleButtonClick(View view) {
    	ToggleButton sampleToggleButton = (ToggleButton) findViewById(R.id.sampleToggleButton);
    	String state = Boolean.toString(sampleToggleButton.isChecked());
    	String message = getResources().getText(R.string.sampleToggleButtonMessage).toString();
    	Toast.makeText(this,  message + " " + state, Toast.LENGTH_SHORT).show();
    }
    
    public void resultsButtonClick(View view) {
    	CheckBox sampleCheckBox = (CheckBox)findViewById(R.id.sampleCheckBox);
    	String somethingToEat = "";
    	if (sampleCheckBox.isChecked())
    		somethingToEat = getResources().getString(R.string.sampleCheckBox) + "\n";
    	
    	String insurance = "";
    	Switch sampleSwitch = (Switch)findViewById(R.id.sampleSwitch);
    	if (sampleSwitch.isChecked())
    		insurance = getResources().getString(R.string.sampleSwitch);
    	
    	Toast.makeText(this, selectedTransport + "\n" + somethingToEat + insurance, Toast.LENGTH_LONG).show();
    }

}
