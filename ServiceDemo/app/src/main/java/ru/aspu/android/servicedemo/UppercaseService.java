package ru.aspu.android.servicedemo;

import java.util.concurrent.TimeUnit;

import android.app.IntentService;
import android.content.Intent;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

public class UppercaseService extends IntentService {

	public UppercaseService() {
		super("ru.aspu.android.demo.UPPERCASE_QUEUE");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// Получаем строку для обработки
		String message = intent.getStringExtra(MainActivity.MESSAGE_EXTRA);
		// Обрабатываем строку и генерируем результат
		String result = message.toUpperCase();
		// Ждем 5 секунд, имитируя сложные вычисления
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// Создаем Message - сообщение для ответа
		Message msg = Message.obtain();
		// помещаем в Message результат
		msg.obj = result;
		// Получаем Messenger и отправляем через него результат обработки
		Messenger messenger = (Messenger) intent.getParcelableExtra(MainActivity.MESSENGER_EXTRA);
		try {
			messenger.send(msg);
		} catch (RemoteException e) {
			e.printStackTrace();
		}
	}

}
