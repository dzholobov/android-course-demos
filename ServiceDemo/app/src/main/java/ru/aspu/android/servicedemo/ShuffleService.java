package ru.aspu.android.servicedemo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.TimeUnit;

import android.app.IntentService;
import android.app.Notification;
import android.app.NotificationManager;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

public class ShuffleService extends IntentService {

	public ShuffleService() {
		super("ru.aspu.android.demo.SHUFFLE_QUEUE");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// Получаем строку для обработки
		String message = intent.getStringExtra(MainActivity.MESSAGE_EXTRA);
		// Обрабатываем строку и генерируем результат
		List<Character> tempList = new ArrayList<Character>();
		for (int i=0; i < message.length(); i++)
			tempList.add(message.charAt(i));
		Collections.shuffle(tempList);
		String result = "";
		for (Character c: tempList)
			result += c;
		// Ждем 5 секунд, имитируя сложные вычисления
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		// Показываем уведомление об окончании обработки
		showNotification(result);

		// Формируем Intent для отправки результата
		Intent resultIntent = new Intent();
		resultIntent.putExtra(MainActivity.MESSAGE_EXTRA, result);
		resultIntent.setAction(MainActivity.SHUFFLE_ACTION);
		// Посылаем широковещательное сообщение с результатом
		sendBroadcast(resultIntent);
	}
	
	private void showNotification(String message) {
		NotificationManager manager = (NotificationManager)getSystemService(NOTIFICATION_SERVICE);

		NotificationCompat.Builder builder = new NotificationCompat.Builder(this);
		builder.setContentTitle(getString(R.string.processingResultText)).
			setContentText(message).
			setSmallIcon(R.drawable.ic_launcher);
		Notification note = builder.build();
		manager.notify(3456, note);

	}

}
