package ru.aspu.android.servicedemo;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import android.app.IntentService;
import android.app.PendingIntent;
import android.app.PendingIntent.CanceledException;
import android.content.Intent;

public class ReverseService extends IntentService {

	public ReverseService() {
		super("ru.aspu.android.demo.REVERSE_QUEUE");
	}

	@Override
	protected void onHandleIntent(Intent intent) {
		// Получаем строку для обработки
		String message = intent.getStringExtra(MainActivity.MESSAGE_EXTRA);
		// Обрабатываем строку и генерируем результат
		String result = "";
		for (int i=0; i < message.length(); i++)
			result = message.charAt(i) + result;
		// Ждем 5 секунд, имитируя сложные вычисления
		try {
			TimeUnit.SECONDS.sleep(5);
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		// Формируем Intent для отправки результата
		Intent resultIntent = new Intent();
		resultIntent.putExtra(MainActivity.MESSAGE_EXTRA, result);
		// получаем PendingIntent и отправляем через него результат
		PendingIntent pendingIntent = intent.getParcelableExtra(MainActivity.PENDING_INTENT_EXTRA);
		try {
			pendingIntent.send(this, 0, resultIntent);
		} catch (CanceledException e) {
			e.printStackTrace();
		}
	}

}
