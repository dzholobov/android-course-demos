package ru.aspu.android.servicedemo;

import android.app.Activity;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends Activity {

	// константы для Extras в Intent, Action
	public static String SHUFFLE_ACTION = "ru.aspu.android.demo.SHUFFLE_ACTION";
	public static String MESSAGE_EXTRA = "ru.aspu.android.demo.MESSAGE_EXTRA";
	public static String MESSENGER_EXTRA = "ru.aspu.android.demo.MESSENGER_EXTRA";
	public static String PENDING_INTENT_EXTRA = "ru.aspu.android.demo.PENDING_INTENT_EXTRA";
	
    private EditText messageEditText;
    private TextView uppercaseResultTextView;
    private TextView reverseResultTextView;
    private TextView shuffleResultTextView;
    
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        messageEditText = (EditText) findViewById(R.id.messageEditText);
        uppercaseResultTextView = (TextView) findViewById(R.id.uppercaseResultTextView);
        reverseResultTextView = (TextView) findViewById(R.id.reverseResultTextView);
        shuffleResultTextView = (TextView) findViewById(R.id.shuffleResultTextView);
    }


	// Перевод к заглавным
	public void uppercaseButtonClick(View view) {
		String message = messageEditText.getText().toString();
		// Формируем Intent и помещаем в него текст для обработки
		Intent intent = new Intent(this, UppercaseService.class);
		intent.putExtra(MESSAGE_EXTRA, message);
		// Создаем новый Handler - обработчик ответов от сервиса
		Handler handler = new Handler() {

			// Получаем ответ через Messenger
			@Override
			public void handleMessage(Message msg) {
				String result = (String)msg.obj;
				uppercaseResultTextView.setText(result);
			}
		};
		// Создаем новый Messenger ("посыльный") и помещаем его в intent
		Messenger messenger = new Messenger(handler);
		intent.putExtra(MESSENGER_EXTRA, messenger);
		uppercaseResultTextView.setText(R.string.processingText);
		// Стартуем сервис
		startService(intent);
	}


	// Разворот строки
	public void reverseButtonClick(View view) {
		String message = messageEditText.getText().toString();
		// Формируем Intent и помещаем в него текст для обработки
		Intent intent = new Intent(this, ReverseService.class);
		intent.putExtra(MESSAGE_EXTRA, message);
		// Создаем PendingIntent (Intent c возвратом) и помещаем его в Intent
		PendingIntent pendingIntent = createPendingResult(0, new Intent(), PendingIntent.FLAG_ONE_SHOT);
		intent.putExtra(PENDING_INTENT_EXTRA, pendingIntent);
		reverseResultTextView.setText(R.string.processingText);
		// Стартуем сервис
		startService(intent);
	}


	// Перемешивание строки
	public void shuffleButtonClick(View view) {
		String message = messageEditText.getText().toString();
		// Формируем Intent и помещаем в него текст для обработки
    	Intent intent = new Intent(this, ShuffleService.class);
		intent.putExtra(MESSAGE_EXTRA, message);

		// Создаем фильтр интентов с нашим Action
		IntentFilter filter = new IntentFilter(SHUFFLE_ACTION);
		// Создаем BroadcastReceiver (получатель широковещательных сообщений)
        BroadcastReceiver rc = new BroadcastReceiver() {

			// получаем ответ через широковещательное сообщение
        	@Override
    		public void onReceive(Context context, Intent intent) {
    			String result = intent.getStringExtra(MESSAGE_EXTRA);
    			shuffleResultTextView.setText(result);
    		}
    	};
		// регистрируем BroadcastReceiver
    	registerReceiver(rc, filter);

		shuffleResultTextView.setText(R.string.processingText);
		// Стартуем сервис
    	startService(intent);
    }


	// Получаем ответ через PendingIntent
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		String result = data.getStringExtra(MESSAGE_EXTRA);
		reverseResultTextView.setText(result);
	}
  
    
}
