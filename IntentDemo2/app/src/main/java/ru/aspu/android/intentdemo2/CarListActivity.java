package ru.aspu.android.intentdemo2;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;
import android.widget.Toast;

public class CarListActivity extends ListActivity {

	static final String CAR_INFO = "ru.aspu.android.intentdemo2.CAR_INFO";

	private boolean resultMode = false;

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_car_list);
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		String car = (String) l.getItemAtPosition(position);

		if (getIntent().getAction().equals(Intent.ACTION_PICK)) {
			Intent result = new Intent(Intent.ACTION_SEND);
			result.setType("text/plain");
			result.putExtra(CAR_INFO, car);
			setResult(RESULT_OK, result);
			finish();
		} else {
			Toast.makeText(this, car, Toast.LENGTH_SHORT).show();
		}
	}

}
