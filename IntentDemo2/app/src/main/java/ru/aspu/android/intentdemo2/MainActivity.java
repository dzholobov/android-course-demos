package ru.aspu.android.intentdemo2;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

public class MainActivity extends Activity {

    static final int SELECT_CAR_REQUEST = 1;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    
	public void selectButtonClick(View view) {
		Intent intent = new Intent(this, CarListActivity.class);
		intent.setAction(Intent.ACTION_PICK);
    	startActivityForResult(intent, SELECT_CAR_REQUEST);
    }

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == SELECT_CAR_REQUEST)
			if (resultCode == RESULT_OK) {
				String selectedCar = data.getStringExtra(CarListActivity.CAR_INFO);
				TextView selectedTextView = (TextView)findViewById(R.id.selectedTextView);
				selectedTextView.setText(selectedCar);
			}
	}
    
	public void showCarListButtonClick(View view) {
    	Intent intent = new Intent(this, CarListActivity.class);
    	intent.setAction(Intent.ACTION_VIEW);
    	startActivity(intent);
    }
    
}
