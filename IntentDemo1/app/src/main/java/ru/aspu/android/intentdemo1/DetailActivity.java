package ru.aspu.android.intentdemo1;

import java.util.List;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;

public class DetailActivity extends Activity {

    private EditText nameEditText;
	private CheckBox primaryCheckBox;
	private TextView carTextView;
	private EditText phoneEditText;

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);
        
        readData();
    }

	private void readData() {
		Intent data = getIntent();
		
		String name = data.getStringExtra(MainActivity.NAME_KEY);
		nameEditText = (EditText)findViewById(R.id.nameEditText);
		nameEditText.setText(name);
		

		boolean primary = data.getBooleanExtra(MainActivity.PRIMARY_KEY, false);
		primaryCheckBox = (CheckBox)findViewById(R.id.primaryCheckBox);
		primaryCheckBox.setChecked(primary);

		String car = data.getStringExtra(MainActivity.CAR_KEY);
		carTextView = (TextView)findViewById(R.id.carTextView);
		carTextView.setText(car);

		String phone = data.getStringExtra(MainActivity.PHONE_KEY);
		phoneEditText = (EditText)findViewById(R.id.phoneEditText);
		phoneEditText.setText(phone);
	}
	
	public void dialButtonClick(View view) {
		String phone = phoneEditText.getText().toString();
		Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + phone));
		startActivity(intent);
	}

	public void sendMessageButtonClick(View view) {
		EditText messageEditText = (EditText)findViewById(R.id.messageEditText);
		String message = messageEditText.getText().toString();
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.putExtra(Intent.EXTRA_TEXT, message);
		intent.setType("text/plain");
		
		PackageManager packageManager = getPackageManager();
		List<ResolveInfo> activities = packageManager.queryIntentActivities(intent, 0);
		if (activities.size() > 0)
			startActivity(Intent.createChooser(intent, ""));
	}
}
