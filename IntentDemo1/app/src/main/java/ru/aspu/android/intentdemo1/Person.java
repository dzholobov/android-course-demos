package ru.aspu.android.intentdemo1;

public class Person {

	private String name;
	private boolean primary;
	private String car;
	private String phone;

	public Person(String name, boolean primary, String car, String phone) {
		super();
		this.name = name;
		this.primary = primary;
		this.car = car;
		this.phone = phone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isPrimary() {
		return primary;
	}

	public void setPrimary(boolean primary) {
		this.primary = primary;
	}

	public String getCar() {
		return car;
	}

	public void setCar(String car) {
		this.car = car;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	@Override
	public String toString() {
		return name;
	}

	
	
}
