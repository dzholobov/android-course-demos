package ru.aspu.android.intentdemo1;

import java.util.ArrayList;
import java.util.List;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends ListActivity {

	public static final String NAME_KEY = "ru.aspu.android.intentdemo1.NAME";
	public static final String PRIMARY_KEY = "ru.aspu.android.intentdemo1.PRIMARY";
	public static final String CAR_KEY = "ru.aspu.android.intentdemo1.CAR";
	public static final String PHONE_KEY = "ru.aspu.android.intentdemo1.PHONE";
	
	private List<Person> persons = new ArrayList<Person>();
	private ArrayAdapter<Person> personsAdapter;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		fillList();
		personsAdapter = new ArrayAdapter<Person>(this, android.R.layout.simple_list_item_1, persons);
		setListAdapter(personsAdapter);
	}

	private void fillList() {
		persons.add(new Person("Иванов", true, "Honda", "+7-555-345-67-89"));
		persons.add(new Person("Петров", true, "Toyota", "+7-555-234-34-77"));
		persons.add(new Person("Сидоров", false, "Honda", "+7-555-382-55-67"));
		persons.add(new Person("Кузнецов", true, "BMW", "+7-555-928-82-28"));
		persons.add(new Person("Аксенов", false, "Dodge", "+7-555-718-71-05"));
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		Person selectedPerson = personsAdapter.getItem(position);
		Intent intent = new Intent(this, DetailActivity.class);
		intent.putExtra(NAME_KEY, selectedPerson.getName());
		intent.putExtra(PRIMARY_KEY, selectedPerson.isPrimary());
		intent.putExtra(CAR_KEY, selectedPerson.getCar());
		intent.putExtra(PHONE_KEY, selectedPerson.getPhone());
		startActivity(intent);
	}
	
	
	
	
}
