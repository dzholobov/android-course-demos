package ru.aspu.android.spinnerdemo;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends Activity {

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        
        Spinner citiesSpinner = (Spinner)findViewById(R.id.citiesSpinner);
        citiesSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				String selectedCity = (String)parent.getItemAtPosition(pos);
				selectCity(selectedCity);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				selectCity("");
			}
		});

    }

	private void selectCity(String city) {
		CharSequence selected = getResources().getText(R.string.selected);
		TextView selectedCityTextView = (TextView)findViewById(R.id.selectedCityTextView);
		selectedCityTextView.setText(selected + " " + city);
	}
	
    
}
