package ru.aspu.android.guibasics3;

import android.app.Activity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {

	class SingleButtonClickListener implements OnClickListener {

		private String text;

		public SingleButtonClickListener(String text) {
			super();
			this.text = text;
		}

		@Override
		public void onClick(View v) {
			setText(text);
		}
	}

	class MultiButtonClickListener implements OnClickListener {

		@Override
		public void onClick(View view) {
			if (view == findViewById(R.id.button6))
				setText("Нажата кнопка 6");
			if (view == findViewById(R.id.button7)) {
				setText("Нажата кнопка 7");
			}
		}

	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		Button button3 = (Button) findViewById(R.id.button3);
		button3.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setText("Нажата кнопка 3");
			}
		});
		
		Button button4 = (Button) findViewById(R.id.button4);
		button4.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				setText("Нажата кнопка 4");
			}
		});

		Button button5 = (Button) findViewById(R.id.button5);
		button5.setOnClickListener(new SingleButtonClickListener(
				"Нажата кнопка 5"));

		MultiButtonClickListener listener = new MultiButtonClickListener();
		Button button6 = (Button) findViewById(R.id.button6);
		button6.setOnClickListener(listener);
		Button button7 = (Button) findViewById(R.id.button7);
		button7.setOnClickListener(listener);
		
		Button button8 = (Button) findViewById(R.id.button8);
		button8.setOnClickListener(this);
		Button button9 = (Button) findViewById(R.id.button9);
		button9.setOnClickListener(this);

	}

	public void setText(String text) {
		TextView textView = (TextView) findViewById(R.id.text_view);
		textView.setText(text);
	}

	public void onButton1Click(View view) {
		setText("Нажата кнопка 1");
	}

	public void onButton2Click(View view) {
		setText("Нажата кнопка 2");
	}

	@Override
	public void onClick(View view) {
		if (view == findViewById(R.id.button8))
			setText("Нажата кнопка 8");
		if (view == findViewById(R.id.button9))
			setText("Нажата кнопка 9");
	}

}
