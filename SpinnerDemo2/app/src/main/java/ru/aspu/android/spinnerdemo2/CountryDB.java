package ru.aspu.android.spinnerdemo2;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CountryDB {

	private Map<Country, List<City>> countries = new HashMap<Country, List<City>>();
	
	public CountryDB() {
		List<City> cities = new ArrayList<City>();
		cities.add(new City(0, "Москва", 10000000));
		cities.add(new City(1, "Санкт-Петербург", 6000000));
		cities.add(new City(2, "Астрахань", 500000));
		countries.put(new Country(0, "Россия"), cities);
		cities = new ArrayList<City>();
		cities.add(new City(3, "Берлин", 12000000));
		cities.add(new City(4, "Мюнхен", 3000000));
		countries.put(new Country(1, "Германия"), cities);
		cities = new ArrayList<City>();
		cities.add(new City(5, "Нью-Йорк", 11000000));
		cities.add(new City(6, "Сан-Франциско", 9000000));
		cities.add(new City(7, "Сиэтл", 2000000));
		countries.put(new Country(2, "США"), cities);
	}
	
	public List<Country> findAllCountries() {
		List<Country> result = new ArrayList<Country>();
		result.addAll(countries.keySet());
		return result;
	}
	
	public List<City> findCitiesByCountry(Country country) {
		return countries.get(country);
	}
	
	
}
