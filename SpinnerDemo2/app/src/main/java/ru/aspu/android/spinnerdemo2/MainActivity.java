package ru.aspu.android.spinnerdemo2;

import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private CountryDB db = new CountryDB();
	private ArrayAdapter<Country> countryAdapter;
	private ArrayAdapter<City> cityAdapter;
	private Spinner countrySpinner;
	private Spinner citySpinner;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buildCountrySpinner();
        buildCitySpinner();
        fillCountrySpinner();
    }

    private void fillCountrySpinner() {
		countryAdapter.clear();
		List<Country> countries = db.findAllCountries();
		countryAdapter.addAll(countries);
	}

    private void buildCountrySpinner() {
		countrySpinner = (Spinner)findViewById(R.id.countrySpinner);
		countrySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				Country country = (Country)parent.getSelectedItem();
				selectCountry(country);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				selectCountry(null);
			}
        	
		});
		countryAdapter = new ArrayAdapter<Country>(this, android.R.layout.simple_spinner_item);
    	countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	countrySpinner.setAdapter(countryAdapter);
	}
    
    private void buildCitySpinner() {
		citySpinner = (Spinner)findViewById(R.id.citySpinner);
        citySpinner.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				City city = (City)parent.getSelectedItem();
				selectCity(city);
			}

			@Override
			public void onNothingSelected(AdapterView<?> parent) {
				selectCity(null);
			}
		});
    	cityAdapter = new ArrayAdapter<City>(this, android.R.layout.simple_spinner_item);
    	cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    	citySpinner.setAdapter(cityAdapter);
	}


    private void selectCountry(Country country) {
    	List<City> cities = db.findCitiesByCountry(country);
    	cityAdapter.clear();
    	cityAdapter.addAll(cities);
    	citySpinner.setSelection(0);
    }
    
    private void selectCity(City city) {
    	TextView populationTextView = (TextView)findViewById(R.id.populationTextView);
    	populationTextView.setText(Integer.toString(city.getPopulation()));
    }
    
    public void resultsButtonClick(View view) {
    	int selectedCountryPosition = countrySpinner.getSelectedItemPosition();
    	Country selectedCountry = countryAdapter.getItem(selectedCountryPosition); 
    	String countryName = selectedCountry.getName();
    	
    	int selectedCityPosition = citySpinner.getSelectedItemPosition();
    	City selectedCity = cityAdapter.getItem(selectedCityPosition); 
    	String cityName = selectedCity.getName();
    	String cityPopulation = Integer.toString(selectedCity.getPopulation());
    	
    	String message = countryName + " \n" + cityName + "\n" + cityPopulation;
    	Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
    
}
