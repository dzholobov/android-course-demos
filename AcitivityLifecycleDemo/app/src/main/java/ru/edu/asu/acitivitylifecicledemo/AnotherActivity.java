package ru.edu.asu.acitivitylifecicledemo;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class AnotherActivity extends LifeCycleNotificationActivity {

   @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_another);
    }

    public void backButtonClick(View v) {
        finish();
    }

    public void dialogButtonClick(View v) {
        AlertDialog dialog = new AlertDialog.Builder(this).
                setTitle("Диалог").
                setMessage("Сообщение").
                setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                }).create();
        dialog.show();
    }


}
