package ru.edu.asu.acitivitylifecicledemo;

import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

public class MainActivity extends LifeCycleNotificationActivity {

    static final String DATA_KEY = "LifeCycleNotificationActivity.Data";

    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void processTextClick(View v) {
        EditText editText = (EditText)findViewById(R.id.editText);
        String text = editText.getText().toString().toUpperCase();
        addText(text);
    }

    public void newActivityClick(View v) {
        Intent intent = new Intent(this, AnotherActivity.class);
        startActivity(intent);
    }

    private void addText(String text) {
        LinearLayout mainLayout = (LinearLayout)findViewById(R.id.mainLayout);
        if (textView == null) {
            textView = new TextView(this);
            mainLayout.addView(textView);
        }
        textView.setText(text);
    }

/*   Раскомментировать для сохранения состояния

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (textView != null)
            outState.putString(DATA_KEY, textView.getText().toString());
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        String text = savedInstanceState.getString(DATA_KEY);
        if (text != null)
            addText(text);
    }
*/
}
