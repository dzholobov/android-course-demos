package ru.edu.asu.acitivitylifecicledemo;

import android.app.Activity;
import android.app.Notification;
import android.app.NotificationManager;
import android.os.Bundle;
import android.util.Log;

/**
 * Created by Rymenar on 12.04.2016.
 */
public class LifeCycleNotificationActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        makeNotification("OnCreate");
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        makeNotification("OnRestoreInstanceState");
    }

    @Override
    protected void onStart() {
        super.onStart();
        makeNotification("OnStart");
    }

    @Override
    protected void onRestart() {
        super.onRestart();
        makeNotification("OnRestart");
    }

    @Override
    protected void onResume() {
        super.onResume();
        makeNotification("OnResume");
    }

    @Override
    protected void onPause() {
        super.onPause();
        makeNotification("OnPause");
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        makeNotification("OnSaveInstanceState");
    }

    @Override
    protected void onStop() {
        super.onStop();
        makeNotification("OnStop");
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        makeNotification("OnDestroy");
    }

    protected void makeNotification(String message) {
        String className = getClass().getSimpleName();
        String text = className + ": " + message;
//        Notification notification = new Notification.Builder(this).
//                setSmallIcon(android.R.drawable.ic_menu_help).
//                setContentTitle(text).
//                setContentText(className).
//                build();
//        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
//        notificationManager.notify((int)System.currentTimeMillis(), notification);
        Log.d("ActivityLifeCycleDemo", text);
    }

}
