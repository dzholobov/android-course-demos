package ru.aspu.android.listdemo;

import java.util.ArrayList;
import java.util.List;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private List<String> cars = new ArrayList<String>();
    private ArrayAdapter<String> itemsAdapter;
	
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buildCarsList();
        buildListView();
    }

	private void buildCarsList() {
		cars.add("BMW");
		cars.add("Audi");
		cars.add("Renault");
		cars.add("Dodge");
		cars.add("Opel");
		cars.add("Honda");
	}

	private void buildListView() {
		ListView itemsListView = (ListView)findViewById(R.id.itemsListView);
        itemsListView.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int pos,
					long id) {
				String selectedCar = parent.getItemAtPosition(pos).toString();
				selectCar(selectedCar);
			}

		});
        itemsAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, cars);
        itemsListView.setAdapter(itemsAdapter);
	}

	private void selectCar(String selectedCar) {
		Toast.makeText(this, selectedCar, Toast.LENGTH_SHORT).show();
	}
	
	public void addItemButtonClick(View view) {
		EditText newItemEditText = (EditText)findViewById(R.id.newItemEditText);
		String newCar = newItemEditText.getText().toString();
		if ((newCar == null) || (newCar.isEmpty()))
				return;
		itemsAdapter.add(newCar);
	}

}
